<?php namespace Devinci\FeaturedPost\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddFeaturedColumn extends Migration
{

	public function up()
	{
		if(Schema::hasTable('rainlab_blog_posts') && ! Schema::hasColumn('rainlab_blog_posts', 'is_featured')){
			Schema::table('rainlab_blog_posts', function ($table) {
				$table->tinyInteger('is_featured')->default(0);
			});
		}
	}

	public function down()
	{
		if(Schema::hasTable('rainlab_blog_posts') && Schema::hasColumn('rainlab_blog_posts', 'is_featured')){
			Schema::table('rainlab_blog_posts', function ($table) {
				$table->dropColumn('is_featured');
			});
		}
	}

}
