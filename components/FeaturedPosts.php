<?php namespace Devinci\Blog\Components;

use Cms\Classes\ComponentBase;
use RainLab\Blog\Models\Post;

class FeaturedPosts extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Featured Posts Component',
            'description' => 'No description provided yet...'
        ];
    }

	public function defineProperties()
	{
		return [
			'limit' => [
				'title'             => 'Limit',
				'description'       => 'Define how many featured posts should display',
				'default'           => 3,
				'validationPattern' => '^[0-9]+$',
				'validationMessage' => 'The Limit property can contain only numeric symbols'
			],
			'show_title' => [
				'title'       => 'Show Title',
				'description' => 'Show the title for the featured section',
				'default'     => true,
				'type'        => 'checkbox',
			],
			'section_title' => [
				'title'       => 'Section Title',
				'description' => 'Add a title to the featured section',
				'default'     => 'Featured Posts',
			]
		];
	}

	public function onRender()
	{
		$this->page['posts']         = Post::whereIsFeatured(true)->get()->take($this->property('limit'));
		$this->page['show_title']    = (bool) $this->property('show_title');
		$this->page['section_title'] = $this->property('section_title');
	}

}
