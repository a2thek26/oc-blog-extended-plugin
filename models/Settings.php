<?php namespace Devinci\Blog\Models;

use Model;

/**
 * Settings Model
 */
class Settings extends Model
{

	public $implement = ['System\Behaviors\SettingsModel'];

	public $settingsCode = 'devinci_blog_settings';

	public $settingsFields = 'fields.yaml';

}
