<?php namespace Devinci\Blog;

use RainLab\Blog\Controllers\Posts as PostsController;
use System\Classes\PluginBase;
use Devinci\Blog\Models\Settings;

/**
 * Blog Plugin Information File
 */
class Plugin extends PluginBase
{

	/**
	 * Required plugins.
	 *
	 * @var array
	 */
	public $require = ['RainLab.Blog'];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
	    return [
		    'name'        => 'Blog',
		    'description' => 'Adds extended functionality to RainLab.Blog',
		    'author'      => 'Devinci',
		    'icon'        => 'icon-star'
	    ];
    }

	/**
	 * @return array
	 */
	public function registerComponents()
	{
		return [
			'Devinci\Blog\Components\FeaturedPosts' => 'featuredPosts',
			'Devinci\Blog\Components\RecentPosts'   => 'recentPosts',
		];
	}

	/**
	 * @return array
	 */
	public function registerSettings()
	{
		return [
			'settings' => [
				'label'       => 'Blog Settings',
				'description' => 'Manage the settings of the blog.',
				'category'    => 'Blog',
				'icon'        => 'icon-cog',
				'class'       => 'Devinci\Blog\Models\Settings',
				'order'       => 500,
				'keywords'    => 'blog settings'
			]
		];
	}

	/**
	 * Boot.
	 */
	public function boot()
	{
		$fields = [
			'is_featured' => [
				'tab'   => 'rainlab.blog::lang.post.tab_manage',
				'label' => 'Featured Post',
				'type'  => 'checkbox'
			]
		];

		if(Settings::instance()->get('use_wysiwyg', false)) {
			$fields['content_html'] = [
				'tab'     => 'rainlab.blog::lang.post.tab_edit',
				'type'    => 'Backend\FormWidgets\RichEditor',
				'stretch' => true
			];
			$fields['preview'] = [
				'tab'    => 'rainlab.blog::lang.post.tab_edit',
				'hidden' => true,
			];

			$fields['content'] = [
				'tab'    => 'rainlab.blog::lang.post.tab_edit',
				'hidden' => true,
			];
		}

		PostsController::extendFormFields(function($form, $model, $context) use ($fields) {
			$form->addSecondaryTabFields($fields);
		});

		PostsController::extendListColumns(function($list, $model){
			$list->addColumns([
				'is_featured' => [
					'label' => 'Featured',
					'type'  => 'switch'
				]
			]);
		});
	}

}
